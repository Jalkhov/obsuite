#!/usr/bin/python3
import os
import setuptools
import setuptools.command.build_py
from PyQt5 import uic
here = os.path.abspath(os.path.dirname(__file__))


class CreateDesktopFile(setuptools.command.build_py.build_py):
  def run(self):
    with open(os.path.join(here + "/obsuite.desktop"), 'w') as f:
        f.write("[Desktop Entry]\n")
        f.write("Name=Openbox Suite\n")
        f.write("GenericName=Openbox tools\n")
        f.write("Terminal=false\n")
        f.write("Type=Application\n")
        f.write("Categories=Utility;\n")
        f.write("Icon=obsuite\n")
        f.write("Exec=obsuite\n")

    with open(os.path.join(here + "/obsuite-preferences.desktop"), 'w') as f:
        f.write("[Desktop Entry]\n")
        f.write("Name=Openbox Suite (preferences)\n")
        f.write("GenericName=Openbox tools\n")
        f.write("Terminal=false\n")
        f.write("Type=Application\n")
        f.write("Categories=Settings;DesktopSettings;\n")
        f.write("Icon=obsuite\n")
        f.write("Exec=obsuite --preferences\n")
    setuptools.command.build_py.build_py.run(self)

uic.compileUiDir('obsuite/ui')
package_data={'': ['ui/sounds/critical.wav', 'ui/icons/*.svg']}
for plugin in ('battery', 'brightness', 'network', 'volume', 'microphone', 'temperature', 'desktop bus', 'splash', 'launcher'):
    package_data[''].append(f'ui/icons/{plugin}/*.svg')
package_data[''].append(f'ui/icons/battery/inhibit/*.svg')

setuptools.setup(
    name='obsuite',
    version='0.0.1',
    description='Collection of system indicators and utilities for custom desktop environments',
    keywords='openbox battery volume temperature backlight network dbus indicator monitor launcher splash',
    author='William Belanger',
    url='https://gitlab.com/william.belanger/obsuite',

    # From https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    cmdclass={'build_py': CreateDesktopFile},
    data_files=[
        ('share/icons/hicolor/scalable/apps', ['obsuite/ui/icons/obsuite.svg']),
        ('share/applications', ['obsuite.desktop']),
        ('share/applications', ['obsuite-preferences.desktop'])
    ],
    package_data=package_data,
    packages=setuptools.find_packages(),
    install_requires=[],
    entry_points={
        'console_scripts': [
            'obsuite=obsuite:main',
        ],
    },
)
