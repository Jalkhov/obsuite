#!/usr/bin/python3
import os
import json
from PyQt5 import QtCore, QtDBus

try:
    from ..backend.common import Indicator
except (ImportError, ValueError):
    from backend.common import Indicator

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'


class Status(QtCore.QObject):
    popupQueued = QtCore.pyqtSignal(str)
    fetch = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.slave = parent.slave
        self.ready = False
        self.lastStatus = False
        self.ignoreConnectionLost = False
        self.link = "wireless"

    def _parse(self, output):
        status = json.loads(output)
        for index, interface in enumerate(status):
            if "lo" in interface["link_type"]:
                del status[index]

        self.online = None
        for interface in status:
            if "LOWER_UP" in interface["flags"]:
                self.link = "wired" if interface["ifname"].startswith("eth") else "wireless"
                self.online = True
                break
        else:
            self.online = False

    def update(self, output):
        self._parse(output)
        if not self.lastStatus == self.online:
            if self.online is None:
                self.icon = "critical"
                self.popupQueued.emit("Network interface not found")
            elif self.online:
                self.icon = f"{self.link}-online"
                self.popupQueued.emit("Connection established")
            else:
                self.icon = f"{self.link}-offline"
                if not self.ignoreConnectionLost:
                    self.popupQueued.emit("Connection lost")
                self.ignoreConnectionLost = False
        self.lastStatus = self.online
        self.ready = True
        self.fetch.emit()


class Main(Indicator):
    def _initHook(self):
        self.status = Status(self)
        self.status.popupQueued.connect(self.popupQueued.emit)

        service = "org.freedesktop.login1"
        if QtDBus.QDBusConnection.systemBus().interface().isServiceRegistered(service).value():
            path = "/org/freedesktop/login1"
            interface = "org.freedesktop.login1.Manager"
            self.bus = QtDBus.QDBusConnection.systemBus()
            self.bus.connect(service, path, interface, "PrepareForSleep", self._disablePopup)

        self.parent.initIndicator(self, "network")

    @QtCore.pyqtSlot()
    def _disablePopup(self):
        self.status.ignoreConnectionLost = True

    def _fetch(self):
        self.slave.fetch("ip -json link")
