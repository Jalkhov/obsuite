#!/usr/bin/python3
import os
from PyQt5 import QtCore

try:
    from ..backend.common import IndicatorIcon, clamp
except (ImportError, ValueError):
    from backend.common import IndicatorIcon, clamp

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'


class Status(QtCore.QObject):
    popup = QtCore.pyqtSignal(str)
    popupQueued = QtCore.pyqtSignal(str)
    fetch = QtCore.pyqtSignal()
    play = QtCore.pyqtSignal()
    criticalTimerStart = QtCore.pyqtSignal(int)
    criticalTimerStop = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.preferences = parent.preferences
        self.popupTimer = QtCore.QTimer(singleShot=True, interval=1000)
        self.popupTimer.timeout.connect(self._showWarningPopup)
        self.ready = True
        self.lastLevel = 0

    def _parse(self):
        thermometer = self.preferences.get("parameters", "temperature", "file")
        with open(thermometer) as f:
            self.level = int(f.read()) / 1000
            self.level = round(self.level)

    def _showWarningPopup(self):
        self.popup.emit(f"{self.level}°C")

    def update(self):
        criticalThreshold = self.preferences.get("parameters", "temperature", "critical threshold")
        highThreshold = self.preferences.get("parameters", "temperature", "high threshold")
        isCritical = self.parent.criticalTimer.isActive()
        self._parse()

        if self.level >= criticalThreshold:
            self.icon = "critical"
        else:
            icon = round(self.level / 5) * 5
            icon = clamp(icon, 0, 100)
            self.icon = str(icon)

        # Temperature is critical
        if not isCritical and self.level >= criticalThreshold:
            delay = self.preferences.get("parameters", "temperature", "critical delay")
            self.play.emit()
            self.criticalTimerStart.emit(delay * 60 * 1000)
            self.popupQueued.emit("Critical threshold reached")

        # Temperature is high
        elif self.level >= highThreshold:
            if self.preferences.get("parameters", "temperature", "high warning"):
                # Use a timer to avoid popup flood
                if not self.popupTimer.isActive():
                    self.popupTimer.start()

        # Temperature is back to normal
        elif isCritical and self.level <= (criticalThreshold - 5):
            self.criticalTimerStop.emit()
            self.critical = False
            self.popupQueued.emit(f"{self.level}°C")

        self.lastLevel = self.level
        self.fetch.emit()


class Main(IndicatorIcon):
    def _initHook(self):
        self.status = Status(self)
        self.criticalTimer = QtCore.QTimer(singleShot=True)
        self.criticalTimer.timeout.connect(self._critical)
        self.status.criticalTimerStop.connect(self.criticalTimer.stop)
        self.status.criticalTimerStart.connect(self.criticalTimer.start)
        self.status.popupQueued.connect(self.popupQueued.emit)
        self.status.popup.connect(self.popup.emit)
        self.parent.initIndicator(self, "temperature")

    def _critical(self):
        if self.preferences.get("parameters", "temperature", "critical"):
            cmd = self.preferences.get("parameters", "temperature", "critical command")
            self.log.warning(f"Execution of '{cmd}'")
            self.slave.start(cmd)
            self.slave.waitForFinished()
            self.fetchTimer.start()  # Reset timer
        else:
            self.log.warning("Critical action ignored")
            delay = self.preferences.get("parameters", "temperature", "critical delay") * 60 * 1000
            self.criticalTimer.start(delay)  # Reset timer
            self.popupQueued.emit(f"Temperature ({self.status.level}°C)")

    def _fetch(self):
        if self._hasThermometer():
            self.status.update()
            self._update()

    def _hasThermometer(self):
        try:
            with open(self.preferences.get("parameters", "temperature", "file")) as f:
                int(f.read())
            return True
        except FileNotFoundError:
            self.error.emit(QtCore.QProcess.FailedToStart)
        except ValueError:
            self.error.emit(QtCore.QProcess.UnknownError)
        except OSError:
            self.log.warning("Unexpected OSError")
        return False

    def _update(self):
        self.setToolTip(f"Temperature: {self.status.lastLevel}°C")
        self.setIcon(self.icons[self.status.icon])

    def action(self, action, command=""):
        if action == "exec":
            self.execute.emit(command)
        else:
            self.log.error(f"Invalid action '{action}'")
