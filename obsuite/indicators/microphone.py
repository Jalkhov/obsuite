#!/usr/bin/python3
import os
import re
from PyQt5 import QtCore

try:
    from ..backend.common import IndicatorIcon, clamp
except (ImportError, ValueError):
    from backend.common import IndicatorIcon, clamp

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'


class Status(QtCore.QObject):
    popup = QtCore.pyqtSignal(str)
    fetch = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.slave = parent.slave
        self.ready = False
        self.lastMute = False
        self.lastLevel = -1

        # Match string such as "Front Left: Capture 19661 [30%] [on]"
        # Capture the level (30) and the state (on)
        self.pattern = re.compile(r"Capture\s\d+\s\[(\d{1,3})%\]\s\[(off|on)\]")

    def _parse(self, stdout):
        match = re.findall(self.pattern, stdout)
        if match:
            try:
                # Put the numer in the first tuple, the on/off state in the second
                state = zip(match[0], match[1])
                state = list(state)
                mute = "on" not in state[1]

                # Calculate the average between left & right channel
                right, left = int(state[0][0]), int(state[0][1])
                level = int((right + left) / 2)
            except IndexError:
                # Only one channel available
                state = match[0]
                mute = "on" not in state
                level = int(state[0][0])
            return level, mute
        return 0, False

    def update(self, output):
        level, mute = self._parse(output)

        if mute:
            self.icon = "mute"
        elif level > 40:
            self.icon = "high"
        elif level > 15:
            self.icon = "medium"
        else:
            self.icon = "low"

        if not self.lastLevel == level:
            if self.lastLevel > -1:
                self.popup.emit(f"{level}%")
        elif not self.lastMute == mute:
            msg = "Mute enabled" if mute else "Mute disabled"
            msg = f"{msg}\n{level}%"
            self.popup.emit(msg)

        self.lastLevel = level
        self.lastMute = mute
        self.ready = True
        self.fetch.emit()


class Main(IndicatorIcon):
    def _initHook(self):
        self.status = Status(self)
        self.status.popup.connect(self.popup.emit)
        self.slave.ready.connect(self._update)
        self.parent.initIndicator(self, "microphone")

        # Match commands such as "amixer -D pulse sset Capture 34%±"
        # Capture the increment (34) and the direction (±)
        self.pattern = re.compile(r".*\s(\d{1,2})%(\+)$|.*\s(\d{1,2})%(-)$")

    def _cutOff(self, cmd, increment, direction):
        minimum = self.preferences.get("parameters", "microphone", "minimum")
        maximum = self.preferences.get("parameters", "microphone", "maximum")
        self.slave.fetch("amixer scontents")
        self.slave.waitForFinished()
        if direction == "+":
            lvl = self.status.lastLevel + increment
        else:
            lvl = self.status.lastLevel - increment
        lvl = clamp(lvl, minimum, maximum)

        end = len(str(increment)) + 2
        cmd = f"{cmd[:-end]}{lvl}%"
        return cmd

    def _fetch(self):
        self.slave.fetch("amixer scontents")

    def _findDelta(self, s):
        delta = re.findall(self.pattern, s)
        if delta:
            # Remove empty elements from the list
            delta = list(filter(None, delta[0]))
            delta[0] = int(delta[0])
            return delta
        return None

    def _update(self, output):
        self.status.update(output)
        self.setToolTip(f"Microphone: {self.status.lastLevel}%")
        self.setIcon(self.icons[self.status.icon])

    def action(self, action, command=""):
        if action == "exec":
            if command.startswith("amixer"):
                try:
                    increment, direction = self._findDelta(command)
                    command = self._cutOff(command, increment, direction)
                except TypeError:
                    pass
            self.execute.emit(command)

        else:
            self.log.error(f"Invalid action '{action}'")
