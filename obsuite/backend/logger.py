#!/usr/bin/python3
import logging
import logging.config
import os
import sys


class Logger:
    def __init__(self, name, path):
        if not os.path.isdir(f"{path}"):
            os.mkdir(f"{path}")
        LOGGER_CONFIG = {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters':
            {
                'default':
                {
                    'format': "[%(asctime)s] [%(levelname)s] [%(name)s] %(message)s",
                    'datefmt': "%H:%M:%S",
                },
            },
            'handlers':
            {
                'file':
                {
                    'level': 'DEBUG',
                    'formatter': 'default',
                    'class': 'logging.FileHandler',
                    'filename': f'{path}{name}.log',
                    'mode': 'w',
                },
                'screen':
                {
                    'level': 'INFO',
                    'formatter': 'default',
                    'class': 'logging.StreamHandler',
                },
            },
            'loggers':
            {
                '':
                {
                    'handlers': ['file', 'screen'],
                    'level': 'DEBUG',
                    'propagate': True,
                },
            }
        }
        logging.config.dictConfig(LOGGER_CONFIG)

    def _exceptHook(self, type_, error, traceback):
        tb = traceback
        logging.critical(f"{type_} {error}".rstrip())
        while "tb_next" in dir(tb):
            logging.critical(f"{tb.tb_frame}")
            tb = tb.tb_next
        sys.__excepthook__(type_, error, traceback)  # Call default hook

    def new(self, name):
        return logging.getLogger(name)
