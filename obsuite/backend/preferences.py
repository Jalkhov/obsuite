#!/usr/bin/python3
import copy
import json
import os
import sys
import xml.etree.ElementTree as ET

from PyQt5 import QtGui, QtWidgets, QtCore, QtDBus, uic
from PyQt5.QtCore import Qt

try:
    from ..__id__ import ID
    from ..ui import preferences
except (ImportError, ValueError):
    from __id__ import ID

# Init common settings
LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'
PREFERENCES_FILE = os.path.expanduser(f"~/.config/{ID}/preferences.json")
HISTORY_FILE = os.path.expanduser(f"~/.config/{ID}/launcher.json")
REPAIR_IGNORED_KEYS = ("splash icons",)

actionsBus = ['Exec']
actionsBattery = ['Exec', 'Reset critical timer', 'Toggle systemd inhibit']
actionsBrightness = ['Exec', 'Toggle brightness level', 'Toggle keyboard backlight', 'Start keyboard backlight', 'Stop keyboard backlight']
actionsVolume = ['Exec', 'Toggle sound level', 'Toggle bluetooth', 'Disconnect bluetooth']
actionsMicrophone = ['Exec']
actionsTemperature = ['Exec']

PREFERENCES_DEFAULT = \
{
    'parameters':
    {
        'battery':
        {
            'enable': False,
            'tray icon': True,
            'sound': True,
            'critical': True,
            'low threshold': 30,
            'critical threshold': 20,
            'critical delay': 5,
            'critical command': 'systemctl suspend',
            'file': [],
        },
        'volume':
        {
            'enable': False,
            'tray icon': True,
            'threshold': 55,
            'low command': 'amixer -D pulse sset Master 30% unmute',
            'high command': 'amixer -D pulse sset Master 55% unmute',
        },
        'microphone':
        {
            'enable': False,
            'tray icon': True,
            'minimum': 0,
            'maximum': 30,
        },
        'brightness':
        {
            'enable': False,
            'tray icon': True,
            'threshold': 60,
            'low command': 'xbacklight -set 5',
            'high command': 'xbacklight -set 100',
            'dim': False,
            'dim level': 0,
            'dim start': '00:00:00',
            'dim end': '05:30:00',
            'minimum': 5,
        },
        'temperature':
        {
            'enable': False,
            'tray icon': False,
            'critical': False,
            'high warning': True,
            'high threshold': 85,
            'critical threshold': 95,
            'critical delay': 1,
            'critical command': 'systemctl suspend',
            'file': '',
        },
        'network':
        {
            'enable': True,
        },
        'bluetooth': {},
        'launcher':
        {
            'startup suggestions': True,
            'close on focus out': True,
            'save draft': True,
            'exec on click': True,
            'desktop files': True,
            'blacklist': False,
            'case sensitive': False,
            'font size': 0,
            'suggestions amount': 10,
        },
        'popup':
        {
            'enable': True,
            'opacity': 0.8,
            'font size': 9,
            'font family': "Sans Serif",
            'width': 150,
            'height': 150,
            'duration': 2,
            'position': 'bottom right',
            'vertical offset': -15,
            'horizontal offset': -20,
            'foreground': 'white',
            'background': 'black',
        },
        'splash':
        {
            'type': 'screenshot',
            'image': '',
            'color': '#000000',
            'alpha': 1.0,
            'grayscale': True,
            'blur': True,
            'blur radius': 5,
            'overlay': True,
            'overlay color': 'black',
            'overlay alpha': 150,
            'colorize': False,
            'colorize color': 'red',
            'colorize strength': 0.2,
            'icon size': 64,
            'enabled icons': ['Suspend', 'Shutdown', 'Restart'],
            'icon label color': '#ffffff',
            'icon label alpha': 230,
            'icon label vertical offset': 40,
            'icon label effect': 'animate',
            'stdout color': '#ffffff',
            'stdout alpha': 220,
            'stdout size': 8,
            'stdout family': 'Bitstream Vera Sans',
        },
        'splash icons':
        {
            'Hibernate': f'{LOCAL_DIR}../ui/icons/splash/hibernate.svg',
            'Suspend': f'{LOCAL_DIR}../ui/icons/splash/suspend.svg',
            'Shutdown': f'{LOCAL_DIR}../ui/icons/splash/shutdown.svg',
            'Restart': f'{LOCAL_DIR}../ui/icons/splash/restart.svg',
            'Logout': f'{LOCAL_DIR}../ui/icons/splash/logout.svg',
            'Lock': f'{LOCAL_DIR}../ui/icons/splash/lock.svg',
        },
    },
    'actions':
    {
        'desktop bus':
        {
            'prepare for shutdown': [False, 'Exec'],
            'prepare for sleep': [False, 'Exec'],
            'resume from sleep': [False, 'Exec'],
            'power connected': [False, 'Exec'],
            'power disconnected': [False, 'Exec'],
        },
        'battery':
        {
            'left click': [True, 'Toggle systemd inhibit'],
            'middle click': [True, 'Reset critical timer'],
            'wheel up': [False, 'None'],
            'wheel down': [False, 'None'],
        },
        'microphone':
        {
            'left click': [True, 'Exec'],
            'middle click': [True, 'Exec'],
            'wheel up': [True, 'Exec'],
            'wheel down': [True, 'Exec'],
        },
        'volume':
        {
            'left click': [True, 'Exec'],
            'middle click': [True, 'Toggle sound level'],
            'wheel up': [True, 'Exec'],
            'wheel down': [True, 'Exec'],
        },
        'brightness':
        {
            'left click': [True, 'Toggle brightness level'],
            'middle click': [True, 'Exec'],
            'wheel up': [True, 'Exec'],
            'wheel down': [True, 'Exec'],
        },
        'temperature':
        {
            'left click': [True, 'Exec'],
            'middle click': [True, 'Exec'],
            'wheel up': [True, 'Exec'],
            'wheel down': [True, 'Exec'],
        },
    },
    'exec':
    {
        'desktop bus':
        {
            'prepare for shutdown': '',
            'prepare for sleep': 'amixer -D pulse sset Master mute',
            'resume from sleep': 'amixer -D pulse sset Master mute',
            'power connected': '',
            'power disconnected': '',
        },
        'battery':
        {
            'left click': '',
            'middle click': '',
            'wheel up': '',
            'wheel down': '',
        },
        'volume':
        {
            'left click': 'amixer -D pulse sset Master toggle',
            'middle click': '',
            'wheel up': 'amixer -D pulse sset Master 2%+ unmute',
            'wheel down': 'amixer -D pulse sset Master 2%- unmute',
        },
        'microphone':
        {
            'left click': 'amixer -D pulse sset Capture toggle',
            'middle click': '',
            'wheel up': 'amixer -D pulse sset Capture 1%+',
            'wheel down': 'amixer -D pulse sset Capture 1%-',
        },
        'brightness':
        {
            'left click': '',
            'middle click': 'xset -display :0.0 dpms force off',
            'wheel up': 'xbacklight -inc 5',
            'wheel down': 'xbacklight -dec 5',
        },
        'temperature':
        {
            'left click': '',
            'middle click': '',
            'wheel up': '',
            'wheel down': '',
        },
        'splash labels':
        {
            'top left': '',
            'top center': '',
            'top right': '',
            'bottom left': 'wmctrl -l',
            'bottom center': '',
            'bottom right': '',
        },
        'splash icons':
        {
            'Hibernate': 'systemctl hibernate',
            'Suspend': 'systemctl suspend',
            'Shutdown': 'systemctl poweroff',
            'Restart': 'systemctl reboot',
            'Logout': 'openbox --exit',
            'Lock': 'xtrlock',
        },
    },
}


class AutoConfig():
    def __init__(self, parent):
        self.log = parent.log

    def _battery(self, db):
        batteries = self._findBatteries()
        db["parameters"]["battery"]["enable"] = bool(batteries)
        if batteries:
            db["parameters"]["battery"]["file"] = batteries
        else:
            self.log.warning("No battery found, module disabled")

    def _brightness(self, db):
        brightnessDeps = self._findDependancies("xorg-xbacklight")
        db["parameters"]["brightness"]["enable"] = brightnessDeps["xorg-xbacklight"]
        if not brightnessDeps["xorg-xbacklight"]:
            self.log.warning("Missing dependancy for brightness (xorg-xbacklight), module disabled")

    def _temperature(self, db):
        thermometers = self._findThermometers()
        for zone in thermometers:
            if thermometers[zone]["type"] == "x86_pkg_temp":
                db["parameters"]["temperature"]["enable"] = True
                db["parameters"]["temperature"]["file"] = zone
                break
        else:
            db["parameters"]["temperature"]["enable"] = False
            self.log.warning("Usual CPU thermal zone not found (x86_pkg_temp), module disabled")
            self.log.warning("The temperature input file must be selected manually under Preferences/Temperature")

    def _volume(self, db):
        soundDeps = self._findDependancies("alsa-utils", "pulseaudio-alsa")
        for dep in soundDeps:
            if not soundDeps[dep]:
                db["parameters"]["volume"]["enable"] = False
                self.log.warning(f"Missing dependancy for volume ({dep}), module disabled")
                break
        else:
            db["parameters"]["volume"]["enable"] = True

    def _findBatteries(self):
        batteries = []
        for batt in os.listdir("/sys/class/power_supply/"):
            if batt.startswith("BAT"):
                batteries.append(f"/sys/class/power_supply/{batt}/uevent")
        return batteries

    def _findDependancies(self, *deps):
        dependancies = {}
        slave = QtCore.QProcess()
        for pkg in deps:
            slave.start(f"pacman -Q {pkg}")
            slave.waitForFinished(3000)
            dependancies[pkg] = bool(slave.readAllStandardOutput())
        return dependancies

    def _findThermometers(self):
        thermometers = {}
        for zone in os.listdir("/sys/class/thermal/"):
            if zone.startswith("thermal_zone"):
                path = f"/sys/class/thermal/{zone}/temp"
                desc = f"/sys/class/thermal/{zone}/type"
                thermometers[path] = {}
                with open(desc) as f:
                    thermometers[path]["type"] = f.read().rstrip()
        return thermometers

    def run(self, db):
        modules = \
        {
            "battery": self._battery,
            "brightness": self._brightness,
            "temperature": self._temperature,
            "volume": self._volume,
        }
        for m in modules:
            try:
                modules[m](db)
            except Exception:
                self.log.error(f"Could not configure {m} module: {sys.exc_info()[0]} {sys.exc_info()[1]}")
        return db


class PreferencesDatabase():
    def __init__(self, parent):
        path = PREFERENCES_FILE
        basename = os.path.basename(path)
        self.name = os.path.splitext(basename)[0]
        self.path = path
        self.log = parent.logger.new(name="Preferences")
        if os.path.isfile(path) and os.stat(path).st_size > 0:
            self.load()
            self._validate()
        else:
            dirname = os.path.dirname(path)
            if not os.path.isdir(dirname):
                os.mkdir(dirname)
            self.db = self.autoConfig()
            with open(path, "w") as f:
                f.write(json.dumps(self.db, indent=2, sort_keys=False))
            self.log.info(f"Created default file for '{self.name}'")

    def _repair(self, db, default=PREFERENCES_DEFAULT):
        for key in default:
            # Set default as an empty type for customizable keys
            if key in db and key in REPAIR_IGNORED_KEYS:
                keyType = type(default[key])
                db.setdefault(key, keyType)

            # Recursively fill missing keys with their default value
            else:
                db.setdefault(key, default[key])
                if isinstance(default[key], dict):
                    self._repair(db[key], default[key])

            # Repair non matching types between current and default db
            if not isinstance(default[key], type(db[key])):
                self.log.warning(f"Fixed type for '{key}': {type(key)} to {type(default[key])}")
                db[key] = default.get(key)

    def _validate(self):
        old = json.dumps(self.db, sort_keys=True)
        self._repair(self.db)
        new = json.dumps(self.db, sort_keys=True)
        if not new == old:
            self.log.warning(f"Repaired missing keys in {self.name} database")
            self.db = self.autoConfig(self.db)
            self.save()

    def autoConfig(self, default=PREFERENCES_DEFAULT):
        self.log.warning("Auto configuration started")
        cfg = AutoConfig(self)
        db = copy.deepcopy(default)
        return cfg.run(db)

    def get(self, *keys):
        db = self.db
        for key in keys:
            try:
                db = db[key]
            except (TypeError, KeyError):
                self.log.error(f"Invalid query {keys} in {self.name} database")
                return None
        return db

    def load(self):
        with open(self.path, "r") as f:
            self.db = json.load(f)
        self.log.info(f"Loaded {self.name} database")

    def save(self):
        with open(self.path, "w") as f:
            f.write(json.dumps(self.db, indent=2, sort_keys=False))
        self.log.info(f"Saved {self.name} database")


class PreferencesForm(QtWidgets.QDialog):
    def __init__(self, parent):
        super().__init__()
        # Load the ui file in case the gui modules are not loaded
        try:
            self.ui = preferences.Ui_Dialog()
            self.ui.setupUi(self)
        except NameError:
            self.ui = uic.loadUi(f"{LOCAL_DIR}../ui/preferences.ui", self)
        self.parent = parent
        self.log = parent.logger.new(name="Preferences")
        self.resize(640, 480)

        self.preferences = parent.preferences
        self.db = copy.deepcopy(self.preferences.db)
        self.ui.resetButton.clicked.connect(self._settingsReset)
        self.ui.buttonBox.button(QtWidgets.QDialogButtonBox.Apply).clicked.connect(self.accepted.emit)
        self.ui.sideMenuList.selectionModel().selectionChanged.connect(self._settingsMenuSelect)
        self.ui.sideMenuList.setFocus(True)
        self.ui.sideMenuList.item(0).setSelected(True)
        self._settingsInit()
        self._settingsLoad()

    def closeEvent(self, event):
        event.ignore()
        self.hide()

    def showEvent(self, event):
        self.db = copy.deepcopy(self.preferences.db)
        self._settingsLoad()
        event.accept()

    def _actionTreeSubItem(self, widget, trigger):
        subItem = QtWidgets.QTreeWidgetItem()
        subItem.setFlags(subItem.flags() | QtCore.Qt.ItemIsEditable)
        subItem.setText(0, trigger.capitalize())
        subItem.setToolTip(0, trigger.capitalize())
        subItem.editableColumn = 2
        subItem.setText(2, self.db["exec"][widget][trigger])
        subItem.setToolTip(2, self.db["exec"][widget][trigger])
        if self.db["actions"][widget][trigger][0]:
            subItem.setCheckState(0, QtCore.Qt.Checked)
        else:
            subItem.setCheckState(0, QtCore.Qt.Unchecked)
        return subItem

    def _actionTreeSubItemCombo(self, widget, trigger):
        action = self.db["actions"][widget][trigger][1]
        width = self.ui.actionTree.header().sectionSize(1) - 12
        combo = QtWidgets.QComboBox(self)
        if widget == "desktop bus":
            combo.addItems(actionsBus)
        elif widget == "battery":
            combo.addItems(actionsBattery)
        elif widget == "brightness":
            combo.addItems(actionsBrightness)
        elif widget == "volume":
            combo.addItems(actionsVolume)
        elif widget == "microphone":
            combo.addItems(actionsMicrophone)
        elif widget == "temperature":
            combo.addItems(actionsTemperature)
        combo.setCurrentText(action)
        combo.setMaximumWidth(width)
        return combo

    def _actionTreeTopItem(self, widget):
        item = QtWidgets.QTreeWidgetItem()
        item.setFlags(item.flags() & ~QtCore.Qt.ItemIsSelectable)
        item.setSizeHint(0, QtCore.QSize(0, 25))
        item.setText(0, widget.title())
        for i in range(3):
            item.setForeground(i, QtGui.QColor("white"))
            item.setBackground(i, QtGui.QColor("black"))
        icon = QtGui.QIcon(f"{LOCAL_DIR}../ui/icons/{widget}/default.svg")
        item.setIcon(0, icon)

        for trigger in self.db["actions"][widget]:
            subItem = self._actionTreeSubItem(widget, trigger)
            combo = self._actionTreeSubItemCombo(widget, trigger)
            item.addChild(subItem)
            self.ui.actionTree.setItemWidget(subItem, 1, combo)
        return item

    def _batteryDetails(self, path):
        brand, health = "Unknown", None
        with open(path) as f:
            uevent = {}
            for line in f.readlines():
                line = line.split("=")
                propriety = line[0][len("POWER_SUPPLY_"):].lower()
                try:
                    uevent[propriety] = int(line[1].rstrip())  # int
                except ValueError:
                    uevent[propriety] = line[1].rstrip().lower()  # string
        if "manufacturer" in uevent:
            brand = uevent["manufacturer"].capitalize()
        if "energy_full" in uevent:
            health = uevent["energy_full"] / uevent["energy_full_design"] * 100
        elif "charge_full" in uevent:
            health = uevent["charge_full"] / uevent["charge_full_design"] * 100
        health = f"{int(health)} %" if health else "?"

        return brand, health

    def _batteriesEnumerate(self):
        self.ui.batteryFileTree.clear()
        batteries = []
        for f in sorted(os.listdir("/sys/class/power_supply/")):
            if f.startswith("BAT"):
                batteries.append(f"/sys/class/power_supply/{f}/uevent")
        for path in batteries:
            item = QtWidgets.QTreeWidgetItem()
            brand, health = self._batteryDetails(path)
            item.setText(0, brand)
            item.setText(1, health)
            item.setText(2, path)
            item.setToolTip(2, path)
            item.setTextAlignment(1, QtCore.Qt.AlignCenter)
            if path in self.db["parameters"]["battery"]["file"]:
                item.setCheckState(0, QtCore.Qt.Checked)
            else:
                item.setCheckState(0, QtCore.Qt.Unchecked)
            self.ui.batteryFileTree.addTopLevelItem(item)

    def _bluetoothEnumerate(self):
        service = "org.bluez"
        root = "/org/bluez"
        interface = "org.freedesktop.DBus.Properties"
        bus = QtDBus.QDBusConnection.systemBus()
        nodes = self._bluetoothNodes()

        # Fill the tree with all trusted bluetooth devices
        self.ui.bluetoothTree.clear()
        for hci in nodes:
            for node in nodes[hci]:
                path = f"{root}/{hci}/{node}"
                connection = QtDBus.QDBusInterface(service, path, interface, bus)
                name = connection.call("Get", "org.bluez.Device1", "Name").arguments()[0]
                address = connection.call("Get", "org.bluez.Device1", "Address").arguments()[0]

                item = QtWidgets.QTreeWidgetItem()
                item.setFlags(item.flags() & ~QtCore.Qt.ItemIsDropEnabled)
                item.setText(0, name)
                item.setText(1, address)
                item.setText(2, path)
                if path not in self.db["parameters"]["bluetooth"]:
                    item.setCheckState(0, QtCore.Qt.Unchecked)
                self.ui.bluetoothTree.addTopLevelItem(item)
        self.ui.bluetoothTree.sortItems(0, QtCore.Qt.AscendingOrder)

        # Put selected devices in the last saved order, on top of the list
        for i, path in enumerate(self.db["parameters"]["bluetooth"]):
            for item in range(self.ui.bluetoothTree.topLevelItemCount()):
                item = self.ui.bluetoothTree.topLevelItem(item)
                if path == item.text(2):
                    index = self.ui.bluetoothTree.indexOfTopLevelItem(item)
                    item = self.ui.bluetoothTree.takeTopLevelItem(index)
                    item.setCheckState(0, QtCore.Qt.Checked)
                    self.ui.bluetoothTree.insertTopLevelItem(i, item)
                    break
        self.ui.bluetoothTree.expandAll()

    def _bluetoothNodes(self):
        service = "org.bluez"
        path = "/org/bluez"
        interface = "org.freedesktop.DBus.Introspectable"
        bus = QtDBus.QDBusConnection.systemBus()
        nodes = {}
        if bus.interface().isServiceRegistered(service).value():
            connection = QtDBus.QDBusInterface(service, path, interface, bus)
            hciNodes = ET.fromstring(connection.call("Introspect").arguments()[0])
            for node in hciNodes.iter("node"):
                if node.attrib:
                    hci = node.attrib["name"]
                    connection = QtDBus.QDBusInterface(service, f"{path}/{hci}", interface, bus)
                    devNodes = ET.fromstring(connection.call("Introspect").arguments()[0])
                    nodes[hci] = []
                    for device in devNodes.iter("node"):
                        if device.attrib:
                            nodes[hci].append(device.attrib["name"])
        return nodes

    def _colorChanged(self, key, layer):
        color = self.colorWidget.currentColor()
        self.db["parameters"][key][layer] = color.name()

    def _launcherKeyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Delete:
            row = self.ui.launcherBlackList.currentRow()
            self.ui.launcherBlackList.takeItem(row)
        QtWidgets.QListWidget.keyPressEvent(self.ui.launcherBlackList, event)

    def _pickColor(self, color, key, layer=None):
        self.colorWidget = QtWidgets.QColorDialog(QtGui.QColor(color))
        self.colorWidget.setWindowFlags(self.colorWidget.windowFlags() | Qt.WindowStaysOnTopHint)
        self.colorWidget.currentColorChanged.connect(lambda: self._colorChanged(key, layer))
        self.colorWidget.exec_()
        return self.colorWidget.selectedColor()

    def _pickLayerColor(self, key, layer):
        currentColor = self.db["parameters"][key][layer]
        color = self._pickColor(currentColor, key, layer)
        if not color.isValid():
            color = QtGui.QColor(currentColor)
        self.db["parameters"][key][layer] = color.name()

    def _settingsInit(self):
        # Actions
        self.ui.actionTree.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.ui.actionTree.itemDoubleClicked.connect(self._treeWidgetItemEdit)

        # Battery
        self.ui.batteryFileTree.setColumnWidth(0, 100)
        self.ui.batteryFileTree.setColumnWidth(1, 50)

        # Temperature
        self.ui.temperatureFileTree.setColumnWidth(0, 100)
        self.ui.temperatureFileTree.setColumnWidth(1, 40)

        # Volume
        self.ui.bluetoothTree.header().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)

        # Notifications
        self.ui.popupBackgroundButton.clicked.connect(lambda: self._pickLayerColor("popup", "background"))
        self.ui.popupForegroundButton.clicked.connect(lambda: self._pickLayerColor("popup", "foreground"))

        # Launcher
        self.ui.launcherBlackList.keyPressEvent = self._launcherKeyPressEvent

        # Splash screen
        self.ui.splashTypeCombo.addItems(("Screenshot", "Image", "Monochrome"))
        self.ui.splashIconEffectCombo.addItems(("Animate", "Show", "Hide"))
        self.ui.splashIconTree.header().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.ui.splashIconTree.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.ui.splashIconTree.itemDoubleClicked.connect(self._treeWidgetItemEdit)
        self.ui.splashIconTree.itemChanged.connect(self._splashIconNameChangedEvent)
        self.ui.splashIconTree.itemSelectionChanged.connect(self._splashIconSelectionChanged)
        self.ui.splashStdoutTree.header().setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        self.ui.splashStdoutTree.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.ui.splashStdoutTree.itemDoubleClicked.connect(self._treeWidgetItemEdit)
        self.ui.splashColorButton.clicked.connect(lambda: self._pickLayerColor("splash", "color"))
        self.ui.splashOverlayColorButton.clicked.connect(lambda: self._pickLayerColor("splash", "overlay color"))
        self.ui.splashStdoutColorButton.clicked.connect(lambda: self._pickLayerColor("splash", "stdout color"))
        self.ui.splashIconColorButton.clicked.connect(lambda: self._pickLayerColor("splash", "icon label color"))
        self.ui.splashColorizeColorButton.clicked.connect(lambda: self._pickLayerColor("splash", "colorize color"))
        self.ui.splashIconAddButton.clicked.connect(self._splashAdd)
        self.ui.splashIconDeleteButton.clicked.connect(self._splashDelete)

    def _settingsLoad(self):
        # Actions
        self.ui.actionTree.clear()
        for widget in self.db["actions"]:
            item = self._actionTreeTopItem(widget)
            self.ui.actionTree.addTopLevelItem(item)
        self.ui.actionTree.expandAll()

        # Network
        self.ui.networkEnableBox.setChecked(self.db["parameters"]["network"]["enable"])

        # Launcher
        self.ui.launcherStartupBox.setChecked(self.db["parameters"]["launcher"]["startup suggestions"])
        self.ui.launcherFocusOutBox.setChecked(self.db["parameters"]["launcher"]["close on focus out"])
        self.ui.launcherSaveDraftBox.setChecked(self.db["parameters"]["launcher"]["save draft"])
        self.ui.launcherClickExecBox.setChecked(self.db["parameters"]["launcher"]["exec on click"])
        self.ui.launcherCaseBox.setChecked(self.db["parameters"]["launcher"]["case sensitive"])
        self.ui.launcherDesktopBox.setChecked(self.db["parameters"]["launcher"]["desktop files"])
        self.ui.launcherBlacklistBox.setChecked(self.db["parameters"]["launcher"]["blacklist"])
        self.ui.launcherAmountBox.setValue(self.db["parameters"]["launcher"]["suggestions amount"])
        self.ui.launcherFontSizeBox.setValue(self.db["parameters"]["launcher"]["font size"])
        self.ui.launcherBlackList.clear()
        try:
            with open(HISTORY_FILE, "r") as f:
                db = json.load(f)
            for line in db.get("blacklist", []):
                self.ui.launcherBlackList.addItem(line)
        except FileNotFoundError:
            pass

        # Battery
        self._batteriesEnumerate()
        self.ui.batteryEnableBox.setChecked(self.db["parameters"]["battery"]["enable"])
        self.ui.batteryTrayIconBox.setChecked(self.db["parameters"]["battery"]["tray icon"])
        self.ui.batterySoundBox.setChecked(self.db["parameters"]["battery"]["sound"])
        self.ui.batteryCriticalBox.setChecked(self.db["parameters"]["battery"]["critical"])
        self.ui.batteryLowThresholdBox.setValue(self.db["parameters"]["battery"]["low threshold"])
        self.ui.batteryCriticalThresholdBox.setValue(self.db["parameters"]["battery"]["critical threshold"])
        self.ui.batteryCriticalDelayBox.setValue(self.db["parameters"]["battery"]["critical delay"])
        self.ui.batteryCriticalCommandLine.setText(self.db["parameters"]["battery"]["critical command"])

        # Temperature
        self._thermometersEnumerate()
        self.ui.temperatureEnableBox.setChecked(self.db["parameters"]["temperature"]["enable"])
        self.ui.temperatureTrayIconBox.setChecked(self.db["parameters"]["temperature"]["tray icon"])
        self.ui.temperatureCriticalBox.setChecked(self.db["parameters"]["temperature"]["critical"])
        self.ui.temperatureWarningBox.setChecked(self.db["parameters"]["temperature"]["high warning"])
        self.ui.temperatureHighThresholdBox.setValue(self.db["parameters"]["temperature"]["high threshold"])
        self.ui.temperatureCriticalThresholdBox.setValue(self.db["parameters"]["temperature"]["critical threshold"])
        self.ui.temperatureCriticalDelayBox.setValue(self.db["parameters"]["temperature"]["critical delay"])
        self.ui.temperatureCriticalCommandLine.setText(self.db["parameters"]["temperature"]["critical command"])

        # Microphone
        self.ui.microphoneEnableBox.setChecked(self.db["parameters"]["microphone"]["enable"])
        self.ui.microphoneTrayIconBox.setChecked(self.db["parameters"]["microphone"]["tray icon"])
        self.ui.microphoneMinBox.setValue(self.db["parameters"]["microphone"]["minimum"])
        self.ui.microphoneMaxBox.setValue(self.db["parameters"]["microphone"]["maximum"])

        # Volume
        self.ui.volumeEnableBox.setChecked(self.db["parameters"]["volume"]["enable"])
        self.ui.volumeTrayIconBox.setChecked(self.db["parameters"]["volume"]["tray icon"])
        self.ui.volumeThresholdBox.setValue(self.db["parameters"]["volume"]["threshold"])
        self.ui.volumeLowCommandLine.setText(self.db["parameters"]["volume"]["low command"])
        self.ui.volumeHighCommandLine.setText(self.db["parameters"]["volume"]["high command"])
        self._bluetoothEnumerate()

        # Brightness
        self.ui.brightnessEnableBox.setChecked(self.db["parameters"]["brightness"]["enable"])
        self.ui.brightnessTrayIconBox.setChecked(self.db["parameters"]["brightness"]["tray icon"])
        self.ui.brightnessThresholdBox.setValue(self.db["parameters"]["brightness"]["threshold"])
        self.ui.brightnessLowCommandLine.setText(self.db["parameters"]["brightness"]["low command"])
        self.ui.brightnessHighCommandLine.setText(self.db["parameters"]["brightness"]["high command"])
        self.ui.brightnessDimBox.setChecked(self.db["parameters"]["brightness"]["dim"])
        self.ui.brightnessDimLevelBox.setValue(self.db["parameters"]["brightness"]["dim level"])
        self.ui.brightnessMinimumBox.setValue(self.db["parameters"]["brightness"]["minimum"])

        time = QtCore.QTime().fromString(self.db["parameters"]["brightness"]["dim start"])
        self.ui.brightnessDimStartBox.setTime(time)
        time = QtCore.QTime().fromString(self.db["parameters"]["brightness"]["dim end"])
        self.ui.brightnessDimEndBox.setTime(time)

        # Notifications
        self.ui.popupEnableBox.setChecked(self.db["parameters"]["popup"]["enable"])
        self.ui.popupFontSizeBox.setValue(self.db["parameters"]["popup"]["font size"])
        self.ui.popupFontFamilyCombo.setCurrentText(self.db["parameters"]["popup"]["font family"])
        self.ui.popupOpacityBox.setValue(self.db["parameters"]["popup"]["opacity"])
        self.ui.popupWidthBox.setValue(self.db["parameters"]["popup"]["width"])
        self.ui.popupHeightBox.setValue(self.db["parameters"]["popup"]["height"])
        self.ui.popupDurationBox.setValue(self.db["parameters"]["popup"]["duration"])
        self.ui.popupVOffsetBox.setValue(self.db["parameters"]["popup"]["vertical offset"])
        self.ui.popupHOffsetBox.setValue(self.db["parameters"]["popup"]["horizontal offset"])

        position = self.db["parameters"]["popup"]["position"]
        if position == "top left":
            self.ui.topLeftRadio.setChecked(True)
        elif position == "top center":
            self.ui.topCenterRadio.setChecked(True)
        elif position == "top right":
            self.ui.topRightRadio.setChecked(True)
        elif position == "middle left":
            self.ui.middleLeftRadio.setChecked(True)
        elif position == "middle center":
            self.ui.middleCenterRadio.setChecked(True)
        elif position == "middle right":
            self.ui.middleRightRadio.setChecked(True)
        elif position == "bottom left":
            self.ui.bottomLeftRadio.setChecked(True)
        elif position == "bottom center":
            self.ui.bottomCenterRadio.setChecked(True)
        elif position == "bottom right":
            self.ui.bottomRightRadio.setChecked(True)

        # Splash screen
        self.ui.splashTypeCombo.setCurrentText(self.db["parameters"]["splash"]["type"])
        self.ui.splashImageLine.setText(self.db["parameters"]["splash"]["image"])
        self.ui.splashAlphaBox.setValue(self.db["parameters"]["splash"]["alpha"])
        self.ui.splashGrayscaleBox.setChecked(self.db["parameters"]["splash"]["grayscale"])
        self.ui.splashBlurBox.setChecked(self.db["parameters"]["splash"]["blur"])
        self.ui.splashBlurRadiusBox.setValue(self.db["parameters"]["splash"]["blur radius"])
        self.ui.splashOverlayBox.setChecked(self.db["parameters"]["splash"]["overlay"])
        self.ui.splashOverlayAlphaBox.setValue(self.db["parameters"]["splash"]["overlay alpha"])
        self.ui.splashColorizeBox.setChecked(self.db["parameters"]["splash"]["colorize"])
        self.ui.splashColorizeStrengthBox.setValue(self.db["parameters"]["splash"]["colorize strength"])
        self.ui.splashIconSizeBox.setValue(self.db["parameters"]["splash"]["icon size"])
        self.ui.splashIconAlphaBox.setValue(self.db["parameters"]["splash"]["icon label alpha"])
        self.ui.splashIconVOffsetBox.setValue(self.db["parameters"]["splash"]["icon label vertical offset"])
        self.ui.splashIconEffectCombo.setCurrentText(self.db["parameters"]["splash"]["icon label effect"])
        self.ui.splashStdoutAlphaBox.setValue(self.db["parameters"]["splash"]["stdout alpha"])
        self.ui.splashStdoutSizeBox.setValue(self.db["parameters"]["splash"]["stdout size"])
        self.ui.splashStdoutFontCombo.setCurrentText(self.db["parameters"]["splash"]["stdout family"])

        self.ui.splashStdoutTree.clear()
        labels = self.db["exec"]["splash labels"]
        for label in labels:
            item = QtWidgets.QTreeWidgetItem()
            item.setText(0, label.capitalize())
            item.setText(1, labels[label])
            item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)
            item.editableColumn = 1
            self.ui.splashStdoutTree.addTopLevelItem(item)
        self.ui.splashStdoutTree.sortItems(0, QtCore.Qt.DescendingOrder)

        self.ui.splashIconTree.clear()
        icons = self.db["parameters"]["splash icons"]
        for name in icons:
            cmd = self.db["exec"]["splash icons"][name]
            icon = icons[name]
            item = self._splashIconTreeItem(name, cmd, icon)
            self.ui.splashIconTree.addTopLevelItem(item)

    def _settingsMenuSelect(self):
        index = self.ui.sideMenuList.currentRow()
        self.ui.stackedWidget.setCurrentIndex(index)

    def _settingsReset(self):
        self.db = self.preferences.autoConfig()
        self._settingsLoad()

    def _splashAdd(self):
        item = self._splashIconTreeItem(name="<blank>", cmd="", icon="")
        self.ui.splashIconTree.addTopLevelItem(item)

    def _splashDelete(self):
        row = self.ui.splashIconTree.currentIndex().row()
        item = self.ui.splashIconTree.currentItem()
        if not item.parent() and row > -1:
            self.ui.splashIconTree.takeTopLevelItem(row)

    def _splashIconNameChangedEvent(self, item, column):
        parent = item.parent()
        if parent and item.text(0) == "Name":
            sender = QtCore.QObject.sender(self)
            oldName = parent.text(0)
            newName = item.text(column)
            for i in range(sender.topLevelItemCount()):
                i = sender.topLevelItem(i)
                if newName == i.text(0):
                    item.setText(column, oldName)
                    break
            else:
                parent.setText(0, newName)

    def _splashIconSelectionChanged(self):
        item = self.ui.splashIconTree.currentItem()
        if item:
            self.ui.splashIconDeleteButton.setEnabled(not item.parent())
        else:
            self.ui.splashIconDeleteButton.setEnabled(False)

    def _splashIconTreeItem(self, name, cmd, icon):
        item = QtWidgets.QTreeWidgetItem()
        item.setText(0, name)
        subItem1 = QtWidgets.QTreeWidgetItem()
        subItem2 = QtWidgets.QTreeWidgetItem()
        subItem3 = QtWidgets.QTreeWidgetItem()
        subItem1.setText(0, "Name")
        subItem1.setText(1, name)
        subItem2.setText(0, "Command")
        subItem2.setText(1, cmd)
        subItem3.setText(0, "Custom icon")
        subItem3.setText(1, icon)
        for i in (item, subItem1, subItem2, subItem2):
            i.setFlags(i.flags() & ~QtCore.Qt.ItemIsDropEnabled)
        for i in (subItem1, subItem2, subItem3):
            i.setFlags(i.flags() | QtCore.Qt.ItemIsEditable)
            i.setFlags(i.flags() & ~QtCore.Qt.ItemIsDragEnabled)
            i.editableColumn = 1
            item.addChild(i)
        if name in self.db["parameters"]["splash"]["enabled icons"]:
            item.setCheckState(0, QtCore.Qt.Checked)
        else:
            item.setCheckState(0, QtCore.Qt.Unchecked)
        return item

    def _thermometerHWFetch(self, path):
        try:
            with open(path) as t:
                temp = int(t.read()) / 1000
                temp = str(round(temp))
        except OSError:
            return None
        try:
            with open(f"{path.rstrip('_input')}_label") as f:
                label = f.read().rstrip()
        except FileNotFoundError:
            label = ""
        return {"label": label, "temp": temp, "type": "hwmon"}

    def _thermometerZoneFetch(self, path):
        try:
            with open(f"{path}/temp") as t:
                temp = int(t.read()) / 1000
                temp = str(round(temp))
        except OSError:
            return None
        with open(f"{path}/type") as f:
            label = f.read().rstrip()
        return {"label": label, "temp": temp, "type": "thermal zone"}

    def _thermometersEnumerate(self):
        probes = self._thermometersFind()
        self.ui.temperatureFileTree.clear()
        for p in probes:
            item = QtWidgets.QTreeWidgetItem()
            item.setText(0, probes[p]["label"])
            item.setText(1, probes[p]["temp"])
            item.setText(2, p)
            item.setToolTip(2, p)
            item.setTextAlignment(1, QtCore.Qt.AlignCenter)
            self.ui.temperatureFileTree.addTopLevelItem(item)
            if p == self.db["parameters"]["temperature"]["file"]:
                self.ui.temperatureFileTree.setCurrentItem(item)

    def _thermometersFind(self):
        probes = {}
        for zone in sorted(os.listdir("/sys/class/thermal/")):
            if zone.startswith("thermal_zone"):
                path = f"/sys/class/thermal/{zone}"
                if self._thermometerZoneFetch(path):
                    probes[f"{path}/temp"] = self._thermometerZoneFetch(path)
        for folder in sorted(os.listdir("/sys/class/hwmon/")):
            root = os.path.join("/sys/class/hwmon", folder)
            for name in sorted(os.listdir(root)):
                if name.startswith("temp") and name.endswith("_input"):
                    path = os.path.join(root, name)
                    if self._thermometerHWFetch(path):
                        probes[path] = self._thermometerHWFetch(path)
        return probes

    def _treeWidgetItemEdit(self, item, column):
        if item.flags() & QtCore.Qt.ItemIsEditable:
            if column == item.editableColumn:
                sender = QtCore.QObject.sender(self)
                sender.editItem(item, column)

    @QtCore.pyqtSlot()
    def settingsSave(self):
        # Actions
        for item in range(self.ui.actionTree.topLevelItemCount()):
            item = self.ui.actionTree.topLevelItem(item)
            widget = item.text(0).lower()
            for subItem in range(item.childCount()):
                subItem = item.child(subItem)
                state = bool(subItem.checkState(0))
                trigger = subItem.text(0).lower()
                combo = self.ui.actionTree.itemWidget(subItem, 1)
                action = combo.currentText()
                command = subItem.text(2)
                self.db["actions"][widget][trigger] = [state, action]
                self.db["exec"][widget][trigger] = command

        # Network
        self.db["parameters"]["network"]["enable"] = self.ui.networkEnableBox.isChecked()

        # Launcher
        self.db["parameters"]["launcher"]["startup suggestions"] = self.ui.launcherStartupBox.isChecked()
        self.db["parameters"]["launcher"]["close on focus out"] = self.ui.launcherFocusOutBox.isChecked()
        self.db["parameters"]["launcher"]["save draft"] = self.ui.launcherSaveDraftBox.isChecked()
        self.db["parameters"]["launcher"]["exec on click"] = self.ui.launcherClickExecBox.isChecked()
        self.db["parameters"]["launcher"]["case sensitive"] = self.ui.launcherCaseBox.isChecked()
        self.db["parameters"]["launcher"]["desktop files"] = self.ui.launcherDesktopBox.isChecked()
        self.db["parameters"]["launcher"]["blacklist"] = self.ui.launcherBlacklistBox.isChecked()
        self.db["parameters"]["launcher"]["suggestions amount"] = self.ui.launcherAmountBox.value()
        self.db["parameters"]["launcher"]["font size"] = self.ui.launcherFontSizeBox.value()

        blacklist = []
        for row in range(self.ui.launcherBlackList.count()):
            item = self.ui.launcherBlackList.item(row)
            blacklist.append(item.text())
        try:
            with open(HISTORY_FILE, "r") as f:
                db = json.load(f)
            with open(HISTORY_FILE, "w") as f:
                db["blacklist"] = blacklist
                f.write(json.dumps(db, indent=2, sort_keys=False))
        except FileNotFoundError:
            pass

        # Battery
        self.db["parameters"]["battery"]["enable"] = self.ui.batteryEnableBox.isChecked()
        self.db["parameters"]["battery"]["tray icon"] = self.ui.batteryTrayIconBox.isChecked()
        self.db["parameters"]["battery"]["sound"] = self.ui.batterySoundBox.isChecked()
        self.db["parameters"]["battery"]["critical"] = self.ui.batteryCriticalBox.isChecked()
        self.db["parameters"]["battery"]["low threshold"] = self.ui.batteryLowThresholdBox.value()
        self.db["parameters"]["battery"]["critical threshold"] = self.ui.batteryCriticalThresholdBox.value()
        self.db["parameters"]["battery"]["critical delay"] = self.ui.batteryCriticalDelayBox.value()
        self.db["parameters"]["battery"]["critical command"] = self.ui.batteryCriticalCommandLine.text()
        self.db["parameters"]["battery"]["file"] = []
        for item in range(self.ui.batteryFileTree.topLevelItemCount()):
            item = self.ui.batteryFileTree.topLevelItem(item)
            if bool(item.checkState(0)):
                self.db["parameters"]["battery"]["file"].append(item.text(2))

        # Temperature
        self.db["parameters"]["temperature"]["enable"] = self.ui.temperatureEnableBox.isChecked()
        self.db["parameters"]["temperature"]["tray icon"] = self.ui.temperatureTrayIconBox.isChecked()
        self.db["parameters"]["temperature"]["critical"] = self.ui.temperatureCriticalBox.isChecked()
        self.db["parameters"]["temperature"]["high warning"] = self.ui.temperatureWarningBox.isChecked()
        self.db["parameters"]["temperature"]["high threshold"] = self.ui.temperatureHighThresholdBox.value()
        self.db["parameters"]["temperature"]["critical threshold"] = self.ui.temperatureCriticalThresholdBox.value()
        self.db["parameters"]["temperature"]["critical delay"] = self.ui.temperatureCriticalDelayBox.value()
        self.db["parameters"]["temperature"]["critical command"] = self.ui.temperatureCriticalCommandLine.text()
        if self.ui.temperatureFileTree.currentItem():
            self.db["parameters"]["temperature"]["file"] = self.ui.temperatureFileTree.currentItem().text(2)

        # Microphone
        self.db["parameters"]["microphone"]["enable"] = self.ui.microphoneEnableBox.isChecked()
        self.db["parameters"]["microphone"]["tray icon"] = self.ui.microphoneTrayIconBox.isChecked()
        self.db["parameters"]["microphone"]["minimum"] = self.ui.microphoneMinBox.value()
        self.db["parameters"]["microphone"]["maximum"] = self.ui.microphoneMaxBox.value()

        # Volume
        self.db["parameters"]["volume"]["enable"] = self.ui.volumeEnableBox.isChecked()
        self.db["parameters"]["volume"]["tray icon"] = self.ui.volumeTrayIconBox.isChecked()
        self.db["parameters"]["volume"]["threshold"] = self.ui.volumeThresholdBox.value()
        self.db["parameters"]["volume"]["low command"] = self.ui.volumeLowCommandLine.text()
        self.db["parameters"]["volume"]["high command"] = self.ui.volumeHighCommandLine.text()
        self.db["parameters"]["bluetooth"] = {}
        for item in range(self.ui.bluetoothTree.topLevelItemCount()):
            item = self.ui.bluetoothTree.topLevelItem(item)
            if bool(item.checkState(0)):
                name = item.text(0)
                path = item.text(2)
                self.db["parameters"]["bluetooth"][path] = name

        # Brightness
        self.db["parameters"]["brightness"]["enable"] = self.ui.brightnessEnableBox.isChecked()
        self.db["parameters"]["brightness"]["tray icon"] = self.ui.brightnessTrayIconBox.isChecked()
        self.db["parameters"]["brightness"]["threshold"] = self.ui.brightnessThresholdBox.value()
        self.db["parameters"]["brightness"]["low command"] = self.ui.brightnessLowCommandLine.text()
        self.db["parameters"]["brightness"]["high command"] = self.ui.brightnessHighCommandLine.text()
        self.db["parameters"]["brightness"]["dim"] = self.ui.brightnessDimBox.isChecked()
        self.db["parameters"]["brightness"]["dim level"] = self.ui.brightnessDimLevelBox.value()
        self.db["parameters"]["brightness"]["dim start"] = self.ui.brightnessDimStartBox.time().toString()
        self.db["parameters"]["brightness"]["dim end"] = self.ui.brightnessDimEndBox.time().toString()
        self.db["parameters"]["brightness"]["minimum"] = self.ui.brightnessMinimumBox.value()

        # Notifications
        self.db["parameters"]["popup"]["enable"] = self.ui.popupEnableBox.isChecked()
        self.db["parameters"]["popup"]["font size"] = self.ui.popupFontSizeBox.value()
        self.db["parameters"]["popup"]["font family"] = self.ui.popupFontFamilyCombo.currentText()
        self.db["parameters"]["popup"]["opacity"] = self.ui.popupOpacityBox.value()
        self.db["parameters"]["popup"]["width"] = self.ui.popupWidthBox.value()
        self.db["parameters"]["popup"]["height"] = self.ui.popupHeightBox.value()
        self.db["parameters"]["popup"]["duration"] = self.ui.popupDurationBox.value()
        self.db["parameters"]["popup"]["vertical offset"] = self.ui.popupVOffsetBox.value()
        self.db["parameters"]["popup"]["horizontal offset"] = self.ui.popupHOffsetBox.value()

        if self.ui.topLeftRadio.isChecked():
            position = "top left"
        elif self.ui.topCenterRadio.isChecked():
            position = "top center"
        elif self.ui.topRightRadio.isChecked():
            position = "top right"
        elif self.ui.middleLeftRadio.isChecked():
            position = "middle left"
        elif self.ui.middleCenterRadio.isChecked():
            position = "middle center"
        elif self.ui.middleRightRadio.isChecked():
            position = "middle right"
        elif self.ui.bottomLeftRadio.isChecked():
            position = "bottom left"
        elif self.ui.bottomCenterRadio.isChecked():
            position = "bottom center"
        elif self.ui.bottomRightRadio.isChecked():
            position = "bottom right"
        self.db["parameters"]["popup"]["position"] = position

        # Splash screen
        self.db["parameters"]["splash"]["type"] = self.ui.splashTypeCombo.currentText().lower()
        self.db["parameters"]["splash"]["image"] = self.ui.splashImageLine.text()
        self.db["parameters"]["splash"]["alpha"] = self.ui.splashAlphaBox.value()
        self.db["parameters"]["splash"]["grayscale"] = self.ui.splashGrayscaleBox.isChecked()
        self.db["parameters"]["splash"]["blur"] = self.ui.splashBlurBox.isChecked()
        self.db["parameters"]["splash"]["blur radius"] = self.ui.splashBlurRadiusBox.value()
        self.db["parameters"]["splash"]["overlay"] = self.ui.splashOverlayBox.isChecked()
        self.db["parameters"]["splash"]["overlay alpha"] = self.ui.splashOverlayAlphaBox.value()
        self.db["parameters"]["splash"]["colorize"] = self.ui.splashColorizeBox.isChecked()
        self.db["parameters"]["splash"]["colorize strength"] = self.ui.splashColorizeStrengthBox.value()
        self.db["parameters"]["splash"]["icon size"] = self.ui.splashIconSizeBox.value()
        self.db["parameters"]["splash"]["icon label alpha"] = self.ui.splashIconAlphaBox.value()
        self.db["parameters"]["splash"]["icon label vertical offset"] = self.ui.splashIconVOffsetBox.value()
        self.db["parameters"]["splash"]["icon label effect"] = self.ui.splashIconEffectCombo.currentText().lower()
        self.db["parameters"]["splash"]["stdout alpha"] = self.ui.splashStdoutAlphaBox.value()
        self.db["parameters"]["splash"]["stdout size"] = self.ui.splashStdoutSizeBox.value()
        self.db["parameters"]["splash"]["stdout family"] = self.ui.splashStdoutFontCombo.currentText()

        for item in range(self.ui.splashStdoutTree.topLevelItemCount()):
            item = self.ui.splashStdoutTree.topLevelItem(item)
            name = item.text(0).lower()
            cmd = item.text(1)
            # Temporary check for obsolete side labels (Legacy, june 2019)
            if name in ("middle left", "middle right"):
                self.db["exec"]["splash labels"].pop(name, None)
            else:
                self.db["exec"]["splash labels"][name] = cmd

        self.db["exec"]["splash icons"] = {}
        self.db["parameters"]["splash icons"] = {}
        splashEnabledIcons = []
        for item in range(self.ui.splashIconTree.topLevelItemCount()):
            item = self.ui.splashIconTree.topLevelItem(item)
            name = item.text(0)
            if bool(item.checkState(0)):
                splashEnabledIcons.append(name)
            for subItem in range(item.childCount()):
                subItem = item.child(subItem)
                if subItem.text(0) == "Command":
                    self.db["exec"]["splash icons"][name] = subItem.text(1)
                elif subItem.text(0) == "Custom icon":
                    self.db["parameters"]["splash icons"][name] = subItem.text(1)
        self.db["parameters"]["splash"]["enabled icons"] = splashEnabledIcons
