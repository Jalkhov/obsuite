#!/usr/bin/python3
from PyQt5 import QtWidgets, QtCore


class Slave(QtCore.QProcess):
    ready = QtCore.pyqtSignal(str)
    error = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super().__init__()
        self.parent = parent
        self.finished.connect(self._output)

    def _output(self):
        stdout = self.readAllStandardOutput()
        stdout = bytes(stdout).decode("utf8")
        if stdout:
            self.ready.emit(stdout)

    def fetch(self, cmd):
        self.lastCommand = cmd
        self.start(cmd)


class Indicator(QtCore.QObject):
    error = QtCore.pyqtSignal(int)
    popup = QtCore.pyqtSignal(str)
    popupQueued = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.preferences = parent.preferences
        self.slave = Slave(self)
        self._initHook()

    def _initHook(self):
        pass


class IndicatorIcon(QtWidgets.QSystemTrayIcon):
    error = QtCore.pyqtSignal(int)
    execute = QtCore.pyqtSignal(str)
    popup = QtCore.pyqtSignal(str)
    popupQueued = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.preferences = parent.preferences
        self.slave = Slave(self)
        self.trayTimer = QtCore.QTimer(singleShot=True, interval=1000)
        self.trayTimer.timeout.connect(self._show)
        self.installEventFilter(self)
        self.activated.connect(self._clickEvent)
        self._initHook()
        if self.preferences.get("parameters", self.name, "tray icon"):
            self._show()

    def _initHook(self):
        pass

    def _clickEvent(self, event):
        self._eventParser(event)

    def _eventParser(self, event):
        trigger = self._trayEventParser(event)
        if trigger:
            enabled = self.preferences.get("actions", self.name, trigger)[0]
            if enabled:
                action = self.preferences.get("actions", self.name, trigger)[1].lower()
                command = self.preferences.get("exec", self.name, trigger)
                self.action(action, command)

    def _show(self):
        if self.isSystemTrayAvailable():
            self.show()
        else:
            self.trayTimer.start()

    def _trayEventParser(self, event):
        if event == QtWidgets.QSystemTrayIcon.Trigger:
            return "left click"

        elif event == QtWidgets.QSystemTrayIcon.MiddleClick:
            return "middle click"

        elif getattr(event, "type", None):
            if event.type() == QtCore.QEvent.Wheel:
                delta = int(event.angleDelta().y())
                if delta > 0:
                    return "wheel up"
                else:
                    return "wheel down"
        return None

    def eventFilter(self, object, event):
        self._eventParser(event)
        return QtWidgets.QSystemTrayIcon.event(self, event)


def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)
