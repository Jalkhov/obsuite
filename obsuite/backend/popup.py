#!/usr/bin/python3
import copy
import os
from PyQt5 import QtGui, QtWidgets, QtCore


class PopupWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.iconLabel = QtWidgets.QLabel()
        self.textLabel = QtWidgets.QLabel()
        self.textLabel.setWordWrap(True)
        self.textLabel.setAlignment(QtCore.Qt.AlignCenter)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.iconLabel)
        layout.addWidget(self.textLabel)
        layout.setAlignment(self.iconLabel, QtCore.Qt.AlignCenter)
        layout.setAlignment(self.textLabel, QtCore.Qt.AlignCenter)
        self.setLayout(layout)
        self.setMouseTracking(True)

        # Hide window from system taskbar
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_ShowWithoutActivating)
        if os.environ.get('DESKTOP_SESSION') == "openbox":
            self.setAttribute(QtCore.Qt.WA_X11NetWmWindowTypeToolBar)
        else:
            self.setAttribute(QtCore.Qt.WA_X11NetWmWindowTypeUtility)

    def mouseMoveEvent(self, event):
        self.hide()

    def mousePressEvent(self, event):
        self.hide()


class Popup(PopupWidget):
    done = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.preferences = parent.preferences
        self.inQueue = False
        self.displayQueue = []
        self.queueTimer = QtCore.QTimer(singleShot=True)
        self.queueTimer.timeout.connect(self._displayQueued)
        self.hideTimer = QtCore.QTimer(singleShot=True)
        self.hideTimer.timeout.connect(self.hide)

    def _display(self, args):
        if self.preferences.get("parameters", "popup", "enable"):
            if "icon" in args:
                width = int(args["width"] * 0.6)
                height = int(args["height"] * 0.6)
                icon = args["icon"].pixmap(width, height)
                self.iconLabel.setPixmap(icon)
            if "msg" in args:
                self.textLabel.setText(args["msg"])

            duration = int(args["duration"] * 1000)
            self._updateView(args)
            self.textLabel.setVisible("msg" in args)
            self.iconLabel.setVisible("icon" in args)
            self.hideTimer.start(duration)
            self.show()

    def _displayQueued(self):
        self._display(self.displayQueue[0])
        del self.displayQueue[0]

    def _getDefaultSettings(self):
        default = copy.deepcopy(self.preferences.get("parameters", "popup"))
        default["mode"] = "instant"
        return default

    def _parse(self, kwargs):
        parameters = self._getDefaultSettings()
        parameters.update(kwargs)
        if "font_size" in parameters:
            parameters["font size"] = parameters["font_size"]
        if "font_family" in parameters:
            parameters["font family"] = parameters["font_family"]
        if "x_offset" in parameters:
            parameters["horizontal offset"] = parameters["x_offset"]
        if "y_offset" in parameters:
            parameters["vertical offset"] = parameters["y_offset"]
        parameters = self._stringToDecimal(parameters)
        return parameters

    def _setFont(self, fontFamily, fontSize):
        font = QtGui.QFont()
        font.setFamily(fontFamily)
        font.setPointSize(fontSize)
        self.textLabel.setFont(font)

    def _setPosition(self, location, xOffset, yOffset):
        available = QtWidgets.QDesktopWidget().availableGeometry()
        position = self.frameGeometry()
        if location == "top left":
            position.moveTopLeft(available.topLeft())
        elif location == "top center":
            position.moveCenter(available.center())
            position.moveTop(available.top())
        elif location == "top right":
            position.moveTopRight(available.topRight())
        elif location == "bottom left":
            position.moveBottomLeft(available.bottomLeft())
        elif location == "bottom center":
            position.moveCenter(available.center())
            position.moveBottom(available.bottom())
        elif location == "bottom right":
            position.moveBottomRight(available.bottomRight())
        elif location == "middle left":
            position.moveCenter(available.center())
            position.moveLeft(available.left())
        elif location == "middle center":
            position.moveCenter(available.center())
        elif location == "middle right":
            position.moveCenter(available.center())
            position.moveRight(available.right())
        x = position.x() + xOffset
        y = position.y() + yOffset
        self.move(x, y)

    def _setStyle(self, foreground, background, opacity):
        fg = QtGui.QColor(foreground)
        bg = QtGui.QColor(background)
        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.WindowText, fg)
        palette.setColor(QtGui.QPalette.Window, bg)
        self.setPalette(palette)
        self.textLabel.setPalette(palette)
        self.setWindowOpacity(opacity)

    def _stringToDecimal(self, parameters):
        for key in ("width", "height", "duration", "opacity", "font size", "horizontal offset", "vertical offset"):
            value = str(parameters[key])
            try:
                parameters[key] = int(value)
            except ValueError:
                try:
                    parameters[key] = float(value)
                except ValueError:
                    parameters[key] = self.preferences.get("parameters", "popup", key)
        return parameters

    def _updateView(self, args):
        width = args["width"]
        height = args["height"]
        foreground = args["foreground"]
        background = args["background"]
        opacity = args["opacity"]
        fontFamily = args["font family"]
        fontSize = args["font size"]
        position = args["position"]
        xOffset = args["horizontal offset"]
        yOffset = args["vertical offset"]
        self.setFixedSize(width, height)
        self._setStyle(foreground, background, opacity)
        self._setFont(fontFamily, fontSize)
        self._setPosition(position, xOffset, yOffset)

    def call(self, **kwargs):
        parameters = self._parse(kwargs)
        if parameters["mode"] == "queue":
            self.displayQueue.append(parameters)
            if not self.inQueue:
                self.inQueue = True
                self.queueTimer.start(0)
        elif not self.inQueue:
            self._display(parameters)

    def hideEvent(self, event):
        self.done.emit()
        if self.displayQueue:
            self.queueTimer.start(500)
        else:
            self.inQueue = False
