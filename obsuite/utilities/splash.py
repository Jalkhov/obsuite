#!/usr/bin/python3
import os
from PyQt5 import QtCore, QtGui, QtWidgets

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'


class ImageFilter():
    def __init__(self, parent):
        super().__init__()

    def _render(self, pixmap, effect):
        scene = QtWidgets.QGraphicsScene()
        item = QtWidgets.QGraphicsPixmapItem(pixmap)
        scene.addItem(item)
        item.setGraphicsEffect(effect)
        image = pixmap.toImage()
        image.fill(QtCore.Qt.transparent)
        painter = QtGui.QPainter(image)
        scene.render(painter)
        painter.end()
        return QtGui.QPixmap(image)

    def blur(self, pixmap, radius):
        effect = QtWidgets.QGraphicsBlurEffect()
        effect.setBlurRadius(radius)
        pixmap = self._render(pixmap, effect)
        return pixmap

    def colorize(self, pixmap, color, strength):
        effect = QtWidgets.QGraphicsColorizeEffect()
        effect.setColor(QtGui.QColor(color))
        effect.setStrength(strength)
        pixmap = self._render(pixmap, effect)
        return pixmap

    def grayscale(self, pixmap):
        image = pixmap.toImage()
        image = image.convertToFormat(QtGui.QImage.Format_Grayscale8)
        pixmap = QtGui.QPixmap(image)
        return pixmap

    def overlay(self, pixmap, color, alpha):
        color = QtGui.QColor(color)
        color.setAlpha(alpha)
        painter = QtGui.QPainter(pixmap)
        painter.fillRect(pixmap.rect(), color)
        painter.end()
        return pixmap


class Main(QtWidgets.QSplashScreen):
    execute = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__()
        self.name = "splash"
        self.parent = parent
        self.preferences = parent.preferences
        self.log = self.parent.logger.new(self.name.capitalize())

        self.cmd = ""
        self.fadeInTimer = QtCore.QTimer(interval=10)
        self.fadeInTimer.timeout.connect(self._fadeIn)
        self.fadeOutTimer = QtCore.QTimer(interval=10)
        self.fadeOutTimer.timeout.connect(self._fadeOut)

        self.setWindowOpacity(0)
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

        self.pixmap = self._getBackground()
        self._setupLayout()
        self.showFullScreen()
        self.fadeInTimer.start()

    def keyReleaseEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self._quit()
        QtWidgets.QLineEdit.keyReleaseEvent(self, event)

    def mousePressEvent(self, event):
        self._quit()

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.setOpacity(self.preferences.get("parameters", "splash", "alpha"))
        if self.pixmap.isNull():
            bgColor = QtGui.QColor(self.preferences.get("parameters", "splash", "color"))
            painter.setPen(QtCore.Qt.NoPen)
            painter.setBrush(QtGui.QBrush(bgColor))
            painter.drawRect(0, 0, self.width(), self.height())
        else:
            painter.drawPixmap(0, 0, self.pixmap)

    def _action(self, action):
        self.cmd = self.preferences.get("exec", "splash icons", action)
        self._quit()

    def _addEffects(self, pixmap):
        mod = ImageFilter(self)
        if self.preferences.get("parameters", "splash", "grayscale"):
            pixmap = mod.grayscale(pixmap)
        if self.preferences.get("parameters", "splash", "colorize"):
            color = self.preferences.get("parameters", "splash", "colorize color")
            strength = self.preferences.get("parameters", "splash", "colorize strength")
            pixmap = mod.colorize(pixmap, color, strength)
        if self.preferences.get("parameters", "splash", "overlay"):
            color = self.preferences.get("parameters", "splash", "overlay color")
            alpha = self.preferences.get("parameters", "splash", "overlay alpha")
            pixmap = mod.overlay(pixmap, color, alpha)
        if self.preferences.get("parameters", "splash", "blur"):
            radius = self.preferences.get("parameters", "splash", "blur radius")
            pixmap = mod.blur(pixmap, radius)
        return pixmap

    def _disableButtons(self):
        for b in self.buttons:
            self.buttons[b].setEnabled(False)

    def _fadeIn(self):
        if self.windowOpacity() < 1.0:
            self.setWindowOpacity(self.windowOpacity() + 0.03)
            self.update()
        else:
            self.fadeInTimer.stop()

    def _fadeOut(self):
        self.fadeInTimer.stop()
        if self.windowOpacity() > 0.1:
            self.setWindowOpacity(self.windowOpacity() - 0.03)
            self.update()
        else:
            self.fadeOutTimer.stop()
            self.close()
            if self.cmd:
                self.execute.emit(self.cmd)

    def _getBackground(self):
        splashType = self.preferences.get("parameters", "splash", "type")
        splashImg = self.preferences.get("parameters", "splash", "image")
        if splashType == "screenshot":
            pixmap = QtWidgets.QApplication.primaryScreen().grabWindow(0)
        elif splashType == "image" and splashImg:
            pixmap = QtGui.QPixmap(splashImg)
            desktop = self.parent.parent.desktop().screenGeometry()
            pixmap = pixmap.scaled(desktop.width(), desktop.height())
        else:
            pixmap = QtGui.QPixmap()  # Null pixmap
            return pixmap
        pixmap = self._addEffects(pixmap)
        return pixmap

    def _quit(self):
        if not self.fadeOutTimer.isActive():
            self._disableButtons()
            self.fadeOutTimer.start()

    def _setupButtons(self):
        self.buttons, self.labels = {}, {}
        for column, option in enumerate(self.preferences.get("parameters", "splash", "enabled icons")):
            self.labels[option] = OptionLabel(self)
            self.labels[option].setText(option.capitalize())
            self.buttons[option] = OptionButton(self)
            self.buttons[option].clicked.connect(lambda x, o=option: self._action(o))
            if self.preferences.get("parameters", "splash", "icon label effect") == "animate":
                self.buttons[option].fadeIn.connect(self.labels[option].fadeIn)
                self.buttons[option].fadeOut.connect(self.labels[option].fadeOut)

            iconPath = self.preferences.get("parameters", "splash icons", option)
            icon = QtGui.QIcon(iconPath)
            self.buttons[option].initIcons(icon)
            self.innerGrid.addWidget(self.buttons[option], 0, column)
            self.innerGrid.addWidget(self.labels[option], 1, column)

    def _setupLabels(self):
        self.topLeftLabel = ProcessLabel(self, self.preferences.get("exec", "splash labels", "top left"))
        self.topCenterLabel = ProcessLabel(self, self.preferences.get("exec", "splash labels", "top center"))
        self.topRightLabel = ProcessLabel(self, self.preferences.get("exec", "splash labels", "top right"))
        self.bottomLeftLabel = ProcessLabel(self, self.preferences.get("exec", "splash labels", "bottom left"))
        self.bottomCenterLabel = ProcessLabel(self, self.preferences.get("exec", "splash labels", "bottom center"))
        self.bottomRightLabel = ProcessLabel(self, self.preferences.get("exec", "splash labels", "bottom right"))
        self.topLeftLabel.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.topCenterLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
        self.topRightLabel.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTop)
        self.bottomLeftLabel.setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignLeft)
        self.bottomCenterLabel.setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignHCenter)
        self.bottomRightLabel.setAlignment(QtCore.Qt.AlignBottom | QtCore.Qt.AlignRight)

    def _setupLayout(self):
        # innerFrame > innerGrid > icons and their labels
        iconSize = self.preferences.get("parameters", "splash", "icon size")
        self.innerFrame = QtWidgets.QFrame(self)
        self.innerGrid = QtWidgets.QGridLayout(self.innerFrame)
        self.innerGrid.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.innerGrid.setHorizontalSpacing(iconSize)
        self._setupButtons()

        # create six expanding labels around the inner frame
        self._setupLabels()
        self.outerFrame = QtWidgets.QFrame(self)
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.outerFrame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.addWidget(self.innerFrame)

        # organize everything into another grid
        self.outerGrid = QtWidgets.QGridLayout(self)
        self.outerGrid.setContentsMargins(9, 9, 9, 0)
        self.outerGrid.addWidget(self.topLeftLabel, 0, 0, 1, 1)
        self.outerGrid.addWidget(self.outerFrame, 1, 0, 1, 3)
        self.outerGrid.addWidget(self.bottomLeftLabel, 2, 0, 1, 1)
        self.outerGrid.addWidget(self.bottomCenterLabel, 2, 1, 1, 1)
        self.outerGrid.addWidget(self.bottomRightLabel, 2, 2, 1, 1)
        self.outerGrid.addWidget(self.topCenterLabel, 0, 1, 1, 1)
        self.outerGrid.addWidget(self.topRightLabel, 0, 2, 1, 1)
        self.setLayout(self.outerGrid)


class OptionButton(QtWidgets.QPushButton):
    fadeIn = QtCore.pyqtSignal()
    fadeOut = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.preferences = parent.preferences
        iconSize = self.preferences.get("parameters", "splash", "icon size")

        self.icons = {}
        self.setFlat(True)
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setFixedSize(iconSize, iconSize)
        self.setIconSize(QtCore.QSize(iconSize, iconSize))
        self.setStyleSheet("QPushButton {border: none;}")
        self.installEventFilter(self)

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.HoverEnter:
            self.setIcon(self.icons["hover"])
            self.fadeIn.emit()
        elif event.type() == QtCore.QEvent.HoverLeave:
            self.setIcon(self.icons["idle"])
            self.fadeOut.emit()
        return QtCore.QObject.event(obj, event)

    def initIcons(self, icon):
        self.icons["idle"] = icon
        self.setIcon(self.icons["idle"])
        iconSize = self.preferences.get("parameters", "splash", "icon size")
        aspectRatio = QtCore.Qt.IgnoreAspectRatio
        smooth = QtCore.Qt.SmoothTransformation
        pixmap = icon.pixmap(QtCore.QSize(iconSize, iconSize))
        pixmap = pixmap.scaled(iconSize, iconSize, aspectRatio, transformMode=smooth)
        painter = QtGui.QPainter(pixmap)
        painter.setCompositionMode(QtGui.QPainter.CompositionMode_HardLight)
        painter.drawPixmap(0, 0, pixmap)
        painter.end()
        self.icons["hover"] = QtGui.QIcon(pixmap)


class OptionLabel(QtWidgets.QLabel):
    def __init__(self, parent):
        super().__init__()
        self.preferences = parent.preferences
        effect = self.preferences.get("parameters", "splash", "icon label effect")
        iconSize = self.preferences.get("parameters", "splash", "icon size")
        self.setWordWrap(True)
        self.setMaximumWidth(iconSize)
        self.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
        self._setColor()

        if effect == "hide":
            self.hide()
        elif effect == "animate":
            self._setAlpha(0)
            self.effect = QtWidgets.QGraphicsOpacityEffect(self)
            self.setGraphicsEffect(self.effect)

    def _setAlpha(self, alpha):
        palette = self.palette()
        color = palette.color(QtGui.QPalette.WindowText)
        color.setAlpha(alpha)
        palette.setColor(QtGui.QPalette.WindowText, color)
        self.setPalette(palette)

    def _setColor(self):
        palette = self.palette()
        color = QtGui.QColor(self.preferences.get("parameters", "splash", "icon label color"))
        palette.setColor(QtGui.QPalette.WindowText, color)
        self.setPalette(palette)

    def fadeIn(self):
        self._setAlpha(self.preferences.get("parameters", "splash", "icon label alpha"))
        self.animation = QtCore.QPropertyAnimation(self.effect, b"opacity")
        self.animation.setDuration(250)
        self.animation.setStartValue(0)
        self.animation.setEndValue(1)
        self.animation.setEasingCurve(QtCore.QEasingCurve.InBack)
        self.animation.start(QtCore.QPropertyAnimation.DeleteWhenStopped)

    def fadeOut(self):
        self.animation = QtCore.QPropertyAnimation(self.effect, b"opacity")
        self.animation.setDuration(500)
        self.animation.setStartValue(1)
        self.animation.setEndValue(0)
        self.animation.setEasingCurve(QtCore.QEasingCurve.OutBack)
        self.animation.start(QtCore.QPropertyAnimation.DeleteWhenStopped)


class ProcessLabel(QtWidgets.QLabel):
    def __init__(self, parent, cmd):
        super().__init__()
        self.parent = parent
        self.preferences = parent.preferences
        self._setFontPalette()
        self._setFontSize()
        self.setWordWrap(True)

        if cmd:
            self.process = QtCore.QProcess()
            self.process.finished.connect(self._display)
            self.process.start(cmd)

    def _display(self):
        stdout = self.process.readAllStandardOutput()
        stdout = bytes(stdout).decode("utf8")
        self.setText(stdout)

    def _setFontPalette(self):
        palette = self.palette()
        color = QtGui.QColor(self.preferences.get("parameters", "splash", "stdout color"))
        color.setAlpha(self.preferences.get("parameters", "splash", "stdout alpha"))
        palette.setColor(QtGui.QPalette.WindowText, color)
        self.setPalette(palette)

    def _setFontSize(self):
        font = self.font()
        font.setFamily(self.preferences.get("parameters", "splash", "stdout family"))
        font.setPointSize(self.preferences.get("parameters", "splash", "stdout size"))
        self.setFont(font)
