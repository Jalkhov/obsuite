#!/usr/bin/python3
import copy
import os
import shlex
import sys
from PyQt5 import QtGui, QtWidgets, QtCore

try:
    from .__id__ import ID
    from .indicators import battery, brightness, bus, microphone, network, temperature, volume
    from .utilities import launcher, splash
    from .backend import cli, popup, preferences, logger
    from .backend.common import IndicatorIcon
except ImportError:
    from __id__ import ID
    from indicators import battery, brightness, bus, microphone, network, temperature, volume
    from utilities import launcher, splash
    from backend import cli, popup, preferences, logger
    from backend.common import IndicatorIcon

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'
CONFIG_DIR = os.path.expanduser(f"~/.config/{ID}/")


class Main(QtCore.QObject):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.logger = logger.Logger(name=ID, path=CONFIG_DIR)
        self.logExec = self.logger.new("Execute")
        self.log = self.logger.new("Main")

        self.splash = QtWidgets.QWidget()
        self.slave = QtCore.QProcess()
        self.slave.setStandardOutputFile(QtCore.QProcess.nullDevice())
        self.slave.setStandardErrorFile(QtCore.QProcess.nullDevice())
        self.preferences = preferences.PreferencesDatabase(self)
        self.preferencesForm = preferences.PreferencesForm(self)
        self.preferencesForm.accepted.connect(self.preferencesSave)
        self.popupWidget = popup.Popup(self)
        self.popupWidget.done.connect(self._setIdle)
        self.launcher = launcher.Main(self)
        self.bus = bus.Main(self)
        self.bus.execute.connect(self.execute)
        self._load()

    def _destroy(self, widget):
        try:
            widget.deleteLater()
        except RuntimeError:
            pass
        if widget.name == "volume":
            widget.bluezThread.quit()
            widget.bluezThread.wait()

    def _load(self):
        self.actives = []
        if self.preferences.get("parameters", "battery", "enable"):
            self.battery = battery.Main(self)
            self.actives.append(self.battery)

        if self.preferences.get("parameters", "volume", "enable"):
            self.volume = volume.Main(self)
            self.actives.append(self.volume)

        if self.preferences.get("parameters", "microphone", "enable"):
            self.microphone = microphone.Main(self)
            self.actives.append(self.microphone)

        if self.preferences.get("parameters", "brightness", "enable"):
            self.brightness = brightness.Main(self)
            self.actives.append(self.brightness)

        if self.preferences.get("parameters", "temperature", "enable"):
            self.temperature = temperature.Main(self)
            self.actives.append(self.temperature)

        if self.preferences.get("parameters", "network", "enable"):
            self.network = network.Main(self)
            self.actives.append(self.network)

        self.loaded = []
        for widget in self.actives:
            self.loaded.append(widget.name)

    def _reload(self):
        for widget in self.actives:
            self._destroy(widget)
        self._load()

    def _setIdle(self):
        for widget in self.actives:
            widget.fetchTimer.setInterval(500)

    @QtCore.pyqtSlot(dict)
    def cli(self, cmd):
        if "quit" in cmd:
            self.parent.exit()

        elif "preferences" in cmd:
            if self.preferencesForm.isVisible():
                self.preferencesForm.hide()
            else:
                self.preferencesForm.show()
                self.preferencesForm.raise_()

        elif "run" in cmd:
            self.launcher.show()

        elif "splash" in cmd:
            if not self.splash.isVisible():
                self.splash = splash.Main(self)
                self.splash.execute.connect(self.execute)

        elif "reload" in cmd:
            self._reload()

        elif "popup" in cmd:
            parameters = {}
            for args in cmd["popup"]:
                args = args.split("=")
                if len(args) > 1:
                    key, value = args
                    if key == "icon":
                        value = QtGui.QIcon(value)
                    parameters[key] = value
            self.popupWidget.call(**parameters)

        elif "action" in cmd:
            if len(cmd["action"]) > 1:
                args = iter(cmd["action"])
                widget, action = next(args), next(args)
                command = next(args, "")
                if widget == "battery" and "battery" in self.loaded:
                    self.battery.action(action, command)
                elif widget == "volume" and "volume" in self.loaded:
                    self.volume.action(action, command)
                elif widget == "microphone" and "microphone" in self.loaded:
                    self.microphone.action(action, command)
                elif widget == "brightness" and "brightness" in self.loaded:
                    self.brightness.action(action, command)
            else:
                self.log.error(f'Correct format: {ID} --action widget "action"')

    def error(self, error):
        widget = QtCore.QObject.sender(self)
        reason = \
        {
            QtCore.QProcess.FailedToStart: "Failed to start",
            QtCore.QProcess.Crashed: "Crashed",
            QtCore.QProcess.Timedout: "Timedout",
            QtCore.QProcess.WriteError: "Write error",
            QtCore.QProcess.ReadError: "Read error",
            QtCore.QProcess.UnknownError: "Unknown error"
        }
        if error in reason:
            error = f"Process error:\n{reason[error]}"
            self._destroy(widget)

        # Log the error and notify the user
        widget.log.critical(error.replace("\n", " "))
        icon = widget.icons["critical"]
        self.popupWidget.call(mode="queue", icon=icon, msg=error)

    def execute(self, cmd):
        if cmd:
            cmd = shlex.split(cmd)
            for i, c in enumerate(cmd):
                cmd[i] = os.path.expanduser(c)
            if self.slave.state() == QtCore.QProcess.NotRunning:
                self.slave.setProgram(cmd[0])
                self.slave.setArguments(cmd[1:])
                self.slave.start()
                self.logExec.info(cmd)

    def initIndicator(self, sender, name):
        sender.name = name
        sender.log = self.logger.new(name.capitalize())

        sender.icons = {}
        for icon in os.listdir(f"{LOCAL_DIR}ui/icons/{name}"):
            basename = os.path.splitext(icon)[0]
            path = f"{LOCAL_DIR}ui/icons/{name}/{icon}"
            sender.icons[basename] = QtGui.QIcon(path)

        if isinstance(sender, IndicatorIcon):
            sender.setIcon(sender.icons["default"])
            sender.execute.connect(self.execute)
        sender.error.connect(self.error)
        sender.popup.connect(self.popup)
        sender.popupQueued.connect(self.popupQueued)

        sender.fetchTimer = QtCore.QTimer(singleShot=True)
        sender.fetchTimer.timeout.connect(sender._fetch)
        sender.fetchTimer.setInterval(500)
        sender.status.fetch.connect(sender.fetchTimer.start)
        sender.slave.ready.connect(sender.status.update)
        sender.slave.errorOccurred.connect(sender.error.emit)
        sender.fetchTimer.start()

    def popup(self, message):
        widget = QtCore.QObject.sender(self)
        if widget.status.ready:
            icon = widget.icons[widget.status.icon]
            self.popupWidget.call(icon=icon, msg=message)

        message = message.replace("\n", ", ")
        widget.log.info(f"{message} (popup)")
        widget.fetchTimer.setInterval(100)

    def popupQueued(self, message):
        widget = QtCore.QObject.sender(self)
        if widget.status.ready:
            icon = widget.icons[widget.status.icon]
            self.popupWidget.call(mode="queue", icon=icon, msg=message)
        message = message.replace("\n", ", ")
        widget.log.info(f"{message} (queued popup)")

    def preferencesActivesChanged(self):
        widgets = ("battery", "brightness", "network", "microphone", "temperature", "volume")
        for w in widgets:
            old = self.preferences.db["parameters"][w]["enable"]
            new = self.preferencesForm.db["parameters"][w]["enable"]
            if not new == old:
                return True
        return False

    def preferencesSave(self):
        self.preferencesForm.settingsSave()
        refresh = self.preferencesActivesChanged()
        self.preferences.db = copy.deepcopy(self.preferencesForm.db)
        self.preferences.save()
        if refresh:
            self._reload()
        else:
            self.updateTrayIcons()
            self.updateBluezDevices()

    def updateBluezDevices(self):
        if "volume" in self.loaded:
            self.volume.bluez.setInterfaces()
            self.volume.initMenu()

    def updateTrayIcons(self):
        for widget in self.actives:
            if isinstance(widget, IndicatorIcon):
                visible = self.preferences.get("parameters", widget.name, "tray icon")
                widget.setVisible(visible)


def main(cmd):
    cmd = cli.parse(cmd)
    app = QtWidgets.QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    widget = Main(app)
    widget.cli(cmd)
    widget.bus = cli.QDBusObject(parent=widget)
    sys.exit(app.exec_())
